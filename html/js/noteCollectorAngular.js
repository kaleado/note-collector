var noteCollectorApp = angular.module('noteCollectorApp', []);

noteCollectorApp.config(['$locationProvider', function($locationProvider) {
    $locationProvider.html5Mode(true);
}]);

noteCollectorApp.controller('NotesController', function NotesController($scope, $http, $location, filterFilter){
    $scope.showTooltip = true;
    var that = $scope;
    that.notes = [];
    $scope.query = $location.search().q;
    $scope.filterNotes = filterFilter($scope.notes, $location.search().q);
    $scope.entriesPerPage = 5;

    $scope.$watch('currentPage', function (val) {
	console.log(val);
	if(val <= 0){$scope.currentPage = 1;}
	if(val > Math.ceil($scope.filterNotes.length/$scope.entriesPerPage)){$scope.currentPage = Math.ceil($scope.filterNotes.length/$scope.entriesPerPage);}
    });
    
    $http.get('notes').then(function(notes){
	that.notes = notes.data;
	$scope.$watch('query', function (term) {
	    console.log(term);
	    $scope.filterNotes = filterFilter($scope.notes, term);
	    $scope.currentPage = 1;
	});
    });
    $scope.range = function(times){
	var arr = [];
	for (var i = 1; i < times; i++) {
	    arr.push(i)
	}
	return arr;
    }
    $scope.setPage = function(pg){
	$scope.currentPage = pg;
	window.scrollTo(0, 0);
	return;
    }
    $scope.orderProperty = "unknown";
    $scope.thumbUp = function (id){
	$http.get("/thumbup/"+id).then(function(){
	    $http.get('notes').then(function(notes){
		that.notes = notes.data;
		$scope.filterNotes = filterFilter($scope.notes, $scope.query);
	    });
	});
	$('.tooltipped').tooltip('remove');
    }
    $scope.thumbDown = function (id){
	$http.get("/thumbdown/"+id).then(function(){
	    $http.get('notes').then(function(notes){
		that.notes = notes.data;
		$scope.filterNotes = filterFilter($scope.notes, $scope.query);
	    });
	});
	$('.tooltipped').tooltip('remove');
    }
}).filter('start', function () {
    return function (input, start) {
	if (!input || !input.length) { return; }

	start = +start;
	return input.slice(start);
    };
});

noteCollectorApp.directive('tooltip', function(){
    return {
	restrict: 'A',
	link: function(scope, element, attrs){
	    $(element).tooltip({delay: 50});
	}
    }
});
