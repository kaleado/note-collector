function httpGet(url, cb){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
	if (xmlHttp.readyState == 4 && (xmlHttp.status == 200 || xmlHttp.status == 0)) {
	    console.log(xmlHttp.responseText);
	    var myArr = JSON.parse(xmlHttp.responseText);
	    console.log('done');
	    cb(myArr);
	}
	else if (xmlHttp.readyState == 4){
	    console.log(xmlHttp.status);
	}
    };
    xmlHttp.open( "GET", url, true );
    xmlHttp.send();
    console.log('sent (GET)');
}


function showComments(element){
    var noteId = element.getAttribute('noteId');
    var commentElmt = "#comments_"+noteId;
    if($(commentElmt).hasClass("hide")){
	$(commentElmt).removeClass("hide");
	$(commentElmt).addClass("animated");
    }
    else{
	$(commentElmt).addClass("hide");
	$(commentElmt).removeClass("animated");
    }
}

function athumbUp(element){
    var id = element.getAttribute('noteId');
    httpGet("/thumbup/"+id);
}

function athumbDown(element){
    var id = element.getAttribute('noteId');
    httpGet("/thumbdown/"+id);
}

$(document).ready(function(){
});

