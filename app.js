/*
Title: note-collector
Author: Nick Malecki, 22/6/2016.
Description: This is a web app for collating all my notes in one place, online.
Not that useful, but kind of fun to make.
Notes:
    - The ./data and ./previews directories must exist before the server is started.
*/

var Moment       = require('moment');
var Crypto       = require("crypto");
var express      = require('express');
var promise      = require('promise');
var bodyParser   = require('body-parser')
var Busboy       = require('busboy');
var fs           = require('fs');
var path         = require('path');
var inspect      = require('util').inspect;
var redis        = require('redis');
var db           = redis.createClient();
var filepreview  = require('filepreview');
var bunyan       = require('bunyan');
var logger = new bunyan({
    name: "NoteCollector",
    streams: [
	{
	    stream: process.stdout,
	    level: "info"
	},
	{
	    stream: process.stdout,
	    level: "error"
	}
    ]
})

var app = express();


logger.info('Creating ./data directory...');
fs.mkdir("./data", undefined, function(e0){logger.error(e0);});

logger.info('Creating ./previews directory...');
fs.mkdir("./previews", undefined, function(e1){logger.error(e1);});

function generateId(note, cb){
    var id;
    id = Crypto.createHash('sha256').update(note.title + note.author + note.timestamp).digest('hex');
    cb(id);
}

function getPreview(id, cb){
    logger.info("Getting preview for " + id + "...");
    getNote(id, function(err0, note){
	if(err0){
	    cb(err0, undefined);
	    return;
	}
	if(note.preview == undefined){
	    if(!note.path){
		logger.error(new Error("Error: note.path undefined."));
		if(cb){cb(new Error("Error: note.path undefined."), undefined);}
		return;
	    }
	    var prevPath = './previews/'+note.filename+".gif";
	    filepreview.generate(note.path, prevPath, function(err1){
		if(err1){
		    logger.error(new Error("An error occurred whilst generating a file preview for " + note.path));
		    if(cb){cb(new Error("An error occurred whilst generating a file preview for " + note.path));}
		    return;
		}
		if(cb){cb(undefined, prevPath);}
	    });
	}
	else {
	    note.preview = prevPath;
	    updateNote(id, note, function(err2){
		if(err2){
		    logger.error(err2);
		    if(cb){cb(err2);}
		    return;
		}
		else{
		    cb(undefined, prevPath);
		}
	    });
	}
    });
}

function getIndex(id, cb){
    getAllNotes(function(undefined, notes){
	for(var i = 0; i < notes.length; i++){
	    if(notes[i].id == id){
		cb(undefined, i);
		return;
	    }
	}
	cb(undefined, -1);
    });
}

function getNote(id, cb){
    getAllNotes(function(err0, notes){
	for(var i = 0; i < notes.length; i++){
	    if(notes[i].id == id){
		cb(undefined, notes[i]);
		return;
	    }
	}
	logger.error(new Error("Error: Note with id " + id + " not found."));
	cb(new Error("Error: Note with id" + id + " not found."));
    });
}

function getAllNotes(cb){
    db.lrange("notes", 0, -1, function(dbErr, notes){
	if(dbErr){
	    logger.error(dbErr);
	    cb(dbErr);
	}
	for(var i = 0; i < notes.length;i++){
	    try{
		notes[i] = JSON.parse(notes[i]);
		notes[i].moment = Moment(notes[i].timestamp).fromNow();
		
	    }
	    catch(parseError){
		logger.error(parseError);
	    }
	}
	cb(undefined, notes);
    });
}

function getNumNotes(cb){
    db.llen("notes", function(dbErr, data){
	if(dbErr){
	    logger.error(dbErr);
	    cb(dbErr);
	}
	cb(undefined, data);
    });    
}

function addNote(note, cb){
    db.lpush("notes", JSON.stringify(note), function(dbErr){
	if(dbErr){
	    logger.error(dbErr);
	    if(cb){cb(dbErr);}
	}
	else{
	    if(cb){cb();}
	}
    });
}

function updateNote(id, newNote, cb){
    getIndex(id, function(err, index){
	if(err){
	    logger.error(err);
	    if(cb){cb(err);}
	}
	db.lset("notes", index, JSON.stringify(newNote), function(dbSetErr){
	    if(dbSetErr){
		logger.error(dbSetErr);
		if(cb){cb(dbSetErr)};
	    }
	    else{
		if(cb){cb();}
	    }
	});
    });
}

function addComment(id, comment, cb){
    getNote(id, function(err, note){
	if(err){
	    logger.error(err);
	    if(cb){cb(err);}
	}
	if(note.comments === undefined){
	    note.comments = [];
	}
	logger.info("Comment posted to " + id + ": '" + comment.body + "', by " + comment.author + ".");
	note.comments.push(comment);
	updateNote(id, note);
	if(cb){cb();}
    });
};

//Log IPs and other identification info.
app.use(function(req, res, next){
    logger.info("Connection from " + req.connection.remoteAddress + ".");
    next();
});

app.use(express.static('./html/'));

app.get('/search/:query', function(req,res){
    var que = req.params.query.toLowerCase();
    var relevantNotes = [];
    getAllNotes(function(err, notes){
	for(var i = 0; i < notes.length; i++){
	    if(notes[i].title.toLowerCase().indexOf(que) != -1 ||
	       notes[i].description.toLowerCase().indexOf(que) != -1 ||
	       notes[i].tags.indexOf(que) != -1){
		relevantNotes.push(notes[i]);
	    }
	}
	res.send(relevantNotes);
    });
});

app.get('/preview/:id', function(req, res){
    logger.info("Note preview for " + req.params.id + " loaded from " + req.connection.remoteAddress + ".");
    getPreview(req.params.id, function(err, prev){
	if(err){
	    logger.error(err);
	    res.sendFile("unavailable.png", {root: './'});
	    return;
	}
	if(prev == undefined){
	    res.sendFile("unavailable.png", {root: './'});
	    //res.status(404).send("Not found");
	    return;
	}
	else{
	    res.sendFile(prev, {root: './'});
	    return;
	}
    });
});

app.get('/comments/:id', function(req, res){
    getNote(req.params.id, function(err0, note){
	var comments = note.comments;
	res.send(comments);
    });
});

app.get('/thumbup/:id', function(req, res){
    getNote(req.params.id, function(err0, note){
	note.thumbup += 1;
	updateNote(req.params.id, note, function(err1){
	    res.writeHead(303, { Connection: 'close', Location: '/' });
	    res.end();
	});
    });
});

app.get('/thumbdown/:id', function(req, res){
    getNote(req.params.id, function(err0, note){
	if(err0){
	    logger.error(err0);
	    res.writeHead(303, { Connection: 'close', Location: '/' });
	    res.end();
	    return;
	}
	note.thumbdown += 1;
	updateNote(req.params.id, note, function(err1){
	    res.writeHead(303, { Connection: 'close', Location: '/' });
	    res.end();
	});
    });
});

app.post('/add_comment/:id', function(req, res){
    logger.info("Comment posted on " + req.params.id + " from " + req.connection.remoteAddress + ".");
    var comment = {
	body: undefined,
	author: undefined,
	timestamp: undefined
    };
    var busboy = new Busboy({ headers: req.headers });
    
    busboy.on('field', function(fieldname, val){
	if(fieldname == "body"){
	    comment.body = val;
	}
	if(fieldname == "author"){
	    comment.author = val;
	}
    });
    busboy.on('finish', function(){
	comment.timestamp = new Date();
	addComment(req.params.id, comment);
	res.writeHead(303, { Connection: 'close', Location: '/' });
	res.end();
    });
    req.pipe(busboy);
});

app.get('toggle_favourite/:id', function(req, res){
    
});

app.get('/num_notes', function(req,res){
    getNumNotes(function(err, numNotes){
	res.send(JSON.stringify({num_notes: numNotes}));
    });
});

app.get('/notes', function(req, res){
    getAllNotes(function(err, notes){
	res.send(notes);
    });
});

app.get('/note/:id', function(req, res){
    getNote(req.params.id, function(err, note){
	res.send(JSON.stringify(note));
    });
});

app.get('/download_note/:id', function(req, res){
    logger.info("Note " + req.params.id + " downloaded from " + req.connection.remoteAddress + ".");
    getNote(req.params.id, function(err, noteObj){
	res.sendFile(noteObj.path, {root: './'});
    });
});

app.get('/delete_note/:id', function(req, res){
    logger.info("Note " + req.params.id + " deleted from " + req.connection.remoteAddress + ".");
    //physically delete the note from the file system.
    getNote(req.params.id, function(err, note){
	fs.unlink(note.path, function(err1){
	    if(err){
		console.log("Error in fs.unlink: " + JSON.stringify(err1));
	    }
	});
    });
    getIndex(req.params.id, function(err, index){
	db.lset("notes", index, "deleted", function(){
	    db.lrem("notes", 0, "deleted");
	});
    });
    res.writeHead(303, { Connection: 'close', Location: '/' });
    res.end();
});

app.post('/update_note/:id', function(req, res){
    logger.info("Note " + req.params.id + " updated from " + req.connection.remoteAddress + ".");
    var busboy = new Busboy({ headers: req.headers });
    var noteObj = {};
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
	if(fieldname == 'title'){
	    noteObject.title = val;
	}
	else if(fieldname == 'tags'){
	    val = val.replace(/\s/g, '');
	    noteObject.tags = val.split(',');
	}
	else if(fieldname == 'description'){
	    noteObject.description = val;
	}
        console.log('Field [' + fieldname + ']: value: ' + inspect(val));
    });
    busboy.on('finish', function() {
	//update the note in the database
	getIndex(req.params.id, function(err, index){
	    db.lset("notes", index, noteObj);
	});
	//we hash the title to produce a unique id for the page.
	console.log('Done parsing form!');
	res.writeHead(303, { Connection: 'close', Location: '/search.html' });
	res.end();
    });
    req.pipe(busboy);
    res.redirect('/search.html')
});

app.post('/create_note', function(req, res){
    var noteObject = {};
    noteObject.size = 0;
    var busboy = new Busboy({ headers: req.headers });
    var id;
    var fieldCount = 0;
    
    busboy.on('file', function(fieldname, file, filename, encoding, mimetype) {
	var fileExt = path.extname(filename);
	noteObject.path = "./data/"+noteObject.id + fileExt;
	noteObject.filename = filename;
	noteObject.encoding = encoding;
	noteObject.mimetype = mimetype;
	noteObject.thumbup = 0;
	noteObject.thumbdown = 0;
	console.log(noteObject.id);

        file.on('data', function(data) {
            fs.appendFileSync("./data/"+noteObject.id + fileExt, data);
	    noteObject.size += data.length;
        });
        file.on('end', function() {
	    console.log(noteObject);
        });
    });
    busboy.on('field', function(fieldname, val, fieldnameTruncated, valTruncated, encoding, mimetype) {
	if(fieldname == 'title'){
	    noteObject.title = val;
	    fieldCount++;
	}
	else if(fieldname == 'author'){
	    noteObject.author = val;
	    fieldCount++;
	}
	else if(fieldname == 'tags'){
	    val = val.replace(/\s/g, '');
	    val = val.replace(/,,+/g, ',');
	    val = val.replace(/,+$/, "");
	    //remove duplicates
	    var unique = val.split(',').filter(function(elem, index, self) {
		return index == self.indexOf(elem);
	    })
	    noteObject.tags = unique;
	    fieldCount++;
	}
	else if(fieldname == 'description'){
	    noteObject.description = val;
	    fieldCount++;
	}
	if(fieldCount >= 4){
	    noteObject.timestamp = new Date();
	    generateId(noteObject, function(newId){
		noteObject.id = newId;
	    });
		    
	}
    });
    busboy.on('finish', function() {
	//we hash the title to produce a unique id for the page.
	addNote(noteObject);
	logger.info("Note " + noteObject.id + " created from " + req.connection.remoteAddress + ".");
	res.writeHead(303, { Connection: 'close', Location: '/search.html' });
	res.end();
    });
    req.pipe(busboy);
});

app.listen(8000);
